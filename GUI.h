#pragma once

#include "opencv2/opencv.hpp"

using namespace cv;
using namespace std;

#define DEBUG_MODE false

void displayImage(const Mat &inImg, const String windowName);
void displayDebugImage(const Mat &inImg, const String windowName);
# Portugal fresco warping

I started working on this OpenCV project soon after my father went back from vacation with a video he took of a long and beautiful fresco. As a challenge, he asked me to "stabilize" the video to make the fresco appear less deformed. 

At this point this is more of a prototype than anything finished, as there would be a lot of things to improve. Nevertheless, let me introduce you to what I did.

## Algorithm principle

The first part of the algorithm is to detect the mask of the fresco, which I do by thresholding the gradient and using basic morphological operations.

The second step is to use the mask to warp the fresco into a fixed height. I do this simply by searching for the top and bottom pixel of the mask in each column, and then stretching it to desired height. There is no stretching in the X axis to compensate for perspective deformations yet (this is another problem that would be interesting to tackle).

## Result

Below are shown the results on two stills from the video, along with the corresponding segmented fresco mask, and the final warped fresco.

![](Images/img0041.png)*First still image from the video*

![](Images/maskImg0041.png)*Fresco mask*

![](Images/warpedImg0041.png)*Warped fresco*

![](Images/img0451.png)*Second still image from the video*

![](Images/maskImg0451.png)*Fresco mask*

![](Images/warpedImg0451.png)*Warped fresco*

#include "Processing.h"

bool processImg(Mat& inImg, Mat& outImg) {
	
	// Checking that input image exists
	if (inImg.empty()) {
		return false;
	}

	// Converting image to grayscale
	Mat imgGray;
	cvtColor(inImg, imgGray, COLOR_BGR2GRAY);

	// Opening to remove small text beneath the fresco
	Mat kernelOpen = Mat::ones(Size(10, 10), CV_8UC1);
	Mat imgGray2;
	morphologyEx(imgGray, imgGray2, MORPH_OPEN, kernelOpen, Point(5, 5));
	
	// Filtering
	Mat imgFilt;
	GaussianBlur(imgGray2, imgFilt, Size(9, 1), 3, 0.5);

	displayDebugImage(imgFilt, "Filtered image");

	// Gradient image along X
	Mat imgGradX;
	Sobel(imgFilt, imgGradX, CV_64F, 1, 0, 5);
	imgGradX = abs(imgGradX);
	normalize(imgGradX, imgGradX, 0, 255, NORM_MINMAX);
	imgGradX.convertTo(imgGradX, CV_8UC1);

	// Gradient image along Y
	Mat imgGradY;
	Sobel(imgFilt, imgGradY, CV_64F, 0, 1, 5);
	imgGradY = abs(imgGradY);
	normalize(imgGradY, imgGradY, 0, 255, NORM_MINMAX);
	imgGradY.convertTo(imgGradY, CV_8UC1);

	// Combining two gradient images
	bitwise_or(imgGradX, imgGradY, imgGradY);

	displayDebugImage(imgGradY, "Combined gradient");

	// Thresholding gradient image
	Mat imgThresh;
	threshold(imgGradY, imgThresh, 15, 255, THRESH_BINARY);
	
	// Converting thresholded image to 8 bits
	Mat imgThresh8UC;
	imgThresh.convertTo(imgThresh8UC, CV_8UC1);

	displayDebugImage(imgThresh8UC, "Threshold");

	// Closing to connect pixels from the fresco
	Mat kernelClose = Mat::ones(Size(15, 5), CV_8UC1);
	Mat imgClosing;
	morphologyEx(imgThresh8UC, imgClosing, MORPH_CLOSE, kernelClose, Point(7, 2));

	drawLineLeftRight(imgClosing, 255);

	displayDebugImage(imgClosing, "Closing");

	// Filling all binary objects
	Mat imgFilled;
	fillHoles(imgClosing, imgFilled);

	displayDebugImage(imgFilled, "Filled");

	// Big opening to separate the fresco from other objects
	Mat kernelOpen2 = Mat::ones(Size(15, 15), CV_8UC1);
	Mat imgOpening;
	morphologyEx(imgFilled, imgOpening, MORPH_OPEN, kernelOpen2, Point(8, 8));

	Mat imgMask;
	keepBiggestObject(imgOpening, imgMask);

	displayDebugImage(imgMask, "Mask");

	imgMask.copyTo(outImg);

	return true;
}


void fillHoles(Mat& inImg, Mat& outImg) {

	// Warning: inImg must be 8 bits!

	Mat tmpImg;
	inImg.copyTo(tmpImg); // we don't want findContours to modify inImg

	// Finding all contours in inImg
	vector<vector<Point>> contours;
	findContours(tmpImg, contours, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);

	// Allocation and filling of output image
	outImg = Mat::zeros(inImg.size(), CV_8UC1);
	drawContours(outImg, contours, -1, Scalar(255), CV_FILLED);
}


// Keeps only the biggest object from a binary image
void keepBiggestObject(Mat& inImg, Mat& outImg) {

	Mat tmpImg;
	inImg.copyTo(tmpImg); // we don't want findContours to modify inImg

	// Finding all contours in inImg
	vector<vector<Point>> contours;
	findContours(tmpImg, contours, RETR_EXTERNAL, CHAIN_APPROX_SIMPLE);

	double maxArea = -1;
	vector<Point> biggestContour;

	// For every object contour
	for (vector<vector<Point>>::iterator curContour = contours.begin(); curContour != contours.end(); curContour++) {
		double curArea = contourArea(*curContour);
		if (curArea > maxArea) {
			maxArea = curArea;
			biggestContour = *curContour;
		}
	}

	vector<vector<Point>> biggestContourList; // vector that will actually contain only 1 element
	biggestContourList.push_back(biggestContour);

	// Allocation and filling of output image
	outImg = Mat::zeros(inImg.size(), CV_8UC1);
	drawContours(outImg, biggestContourList, -1, Scalar(255), CV_FILLED);
}


bool columnWiseWarping(Mat &inImgToWarp, Mat &inImgMask, Mat &outImg) {
	
	if (inImgMask.empty() || inImgToWarp.empty()) {
		return false;
	}

	outImg = Mat::zeros(inImgToWarp.size(), inImgToWarp.type());

	int warpedFirstPoint = 200;
	int warpedLastPoint = 800;

	// For each column
	for (int curCol = 0; curCol < inImgMask.cols; curCol++) {
		int firstPoint = -1, lastPoint = -1;
		// For each line
		for (int curLine = 0; curLine < inImgMask.rows; curLine++) {
			
			// Getting pixel value
			uchar curPix = inImgMask.at<uchar>(curLine, curCol);

			if (curPix != 0) {
				int x = 0;
			}

			if (curPix !=0 && firstPoint == -1) {
				// This is the first point
				firstPoint = curLine;
			}
			else if (curPix != 0) {
				// This is the last point
				lastPoint = curLine;
			}
		}

		if (firstPoint != -1 && lastPoint != -1) {
			// Warping of current column
			Mat tmpImg;
			resize(inImgToWarp(Rect(curCol, firstPoint, 1, lastPoint - firstPoint)), tmpImg, Size(1, warpedLastPoint - warpedFirstPoint));
			tmpImg.copyTo(outImg(Rect(curCol, warpedFirstPoint, 1, warpedLastPoint - warpedFirstPoint)));

			// Reinitialization for next column
			firstPoint = -1;
			lastPoint = -1;
		}
	}

	return true;
}


bool drawLineLeftRight(Mat &inOutImg, uchar inVal) {

	if (inOutImg.empty()) {
		return false;
	}

	int lineWidth = 5;

	// Allocation of column imaage
	Mat colImg = Mat::ones(inOutImg.rows, lineWidth, inOutImg.type()) * inVal;

	// Copying column image on the left and right of inImg
	colImg.copyTo(inOutImg(Rect(0, 0, lineWidth, inOutImg.rows)));
	colImg.copyTo(inOutImg(Rect(inOutImg.cols - lineWidth, 0, lineWidth, inOutImg.rows)));

	return true;
}
#include "GUI.h"

void displayImage(const Mat &inImg, const String windowName) {
	imshow(windowName, inImg);
	waitKey(0);
}

void displayDebugImage(const Mat &inImg, const String windowName) {
	if (DEBUG_MODE) {
		displayImage(inImg, windowName);
	}
}

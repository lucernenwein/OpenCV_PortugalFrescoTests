
#include "TestVideoPortugal.h"
#include "Processing.h"

#define delimdir "/"
// #define delimdir "\\"

int main(int argc, char** argv) {

	long firstIndex = 1, lastIndex = 500, stepIndex = 50;
	Mat inputImg, resImg;
//	string workDir = "C:\\Users\\Luc\\Documents\\Visual Studio 2015\\Projects\\TestVideoPortugal\\TestVideoPortugal\\Test files\\Input video";
	string workDir = ".";
	long nbImages = (lastIndex - firstIndex) / stepIndex + 1;

	for (int i = firstIndex; i <= lastIndex; i = i + stepIndex) {

		// Padding current index with leading zeros
		ostringstream ss;
		ss << setw(4) << setfill('0') << i;
		string paddedIndex = ss.str();
		
		string imagePath = workDir + delimdir + "image" + paddedIndex + ".png";
		inputImg = imread(imagePath, CV_LOAD_IMAGE_COLOR);

		// Processing image
		if (!processImg(inputImg, resImg)) {
			cout << "Could not load or process image " << imagePath << "." << endl;
			return EXIT_FAILURE;
		}

		imwrite(workDir + delimdir + "maskImg" + paddedIndex + ".png", resImg);

		// Warping of input image
		Mat warpedImg;
		columnWiseWarping(inputImg, resImg, warpedImg);
		imwrite(workDir + delimdir + "warpedImg" + paddedIndex + ".png", warpedImg);

		// Completion display (%)
		double percentDone = 100.* (i / stepIndex) / nbImages;
		cout << percentDone << "%..." <<  endl;
	}

	cout << "Done." << endl;

	return EXIT_SUCCESS;
}

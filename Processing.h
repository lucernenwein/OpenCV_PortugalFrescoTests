#pragma once

#include "opencv2/opencv.hpp"

#include "GUI.h"

using namespace std;
using namespace cv;

bool processImg(Mat& inImg, Mat& outImg);
void fillHoles(Mat& inImg, Mat& outImg);
void keepBiggestObject(Mat& inImg, Mat& outImg);
bool columnWiseWarping(Mat &inImgToWarp, Mat &inImgMask, Mat &outImg);
bool drawLineLeftRight(Mat &inOutImg, uchar inVal);
